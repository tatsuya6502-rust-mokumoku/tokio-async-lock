use futures::Future;
use log::debug;
use rand::Rng;
use std::error::Error;
use std::sync::{Arc, Mutex};
use std::thread;
use std::time::{Duration, Instant};
use tokio::prelude::Async;
use tokio::runtime::{Builder as RuntimeBuilder, Runtime};
use tokio::timer::Delay;

fn main() -> Result<(), Box<dyn Error>> {
    env_logger::init();

    // Number of concurrent async updater tasks to run.
    const NUM_TASKS: u8 = 10;

    // Create the (unprotected) shared u8 value. 
    // NOTE: We use std::sync::Mutex here just to avoid a compile error (Sync bound
    // is required.)  We will not use this mutex to protect the data.
    let shared_value = Arc::new(Mutex::new(0));

    // Create async update tasks.
    let mut tasks = (0..NUM_TASKS)
        .map(|i| update_task(i, Arc::clone(&shared_value)))
        .collect::<Vec<_>>();
    let last_task = tasks.pop().ok_or_else(|| "Failed to get the last task")?;

    // Run the async update tasks in parallel. (This will mess up the shared value)
    let mut runtime = build_runtime()?;
    tasks.into_iter().for_each(|task| {
        runtime.spawn(task);
    });
    runtime
        .block_on_all(last_task)
        .map_err(|()| "Failed to run updaters")?;

    // Verify the final value.
    let final_val = shared_value.lock().map_err(|e| e.to_string())?.clone();
    debug!("The final value is {}.", final_val);
    assert_eq!(final_val, NUM_TASKS);

    Ok(())
}

fn build_runtime() -> Result<Runtime, Box<dyn Error>> {
    RuntimeBuilder::new()
        .panic_handler(|err| std::panic::resume_unwind(err))
        .build()
        .map_err(|err| err.into())
}

fn update_task(task_id: u8, shared_value: Arc<Mutex<u8>>) -> impl Future<Item = (), Error = ()> {
    MyDataUpdater::new(task_id, shared_value)
        .and_then(move |shared_value| {  // need to move the task_id into this closure.
        // Get the shared u8 value. (Note that we are releasing the lock here)
        let v0 = *shared_value.lock().unwrap();
        debug!(
            "{:?} Task({}) - Got the current value: {:?}",
            thread::current().id(),
            task_id,
            v0,
        );

        // Do a time-consuming async work.
        let delay_millis = rand::thread_rng().gen_range(50, 100);
        Delay::new(Instant::now() + Duration::from_millis(delay_millis))
            .map_err(|_err| ())
            .and_then(move |_| {
                // Once the async work is done, update the shared u8 value
                // using the former value retrieved sometime ago.
                let v1 = v0 + 1;
                *shared_value.lock().unwrap() = v1;
                debug!(
                    "{:?} Task({}) - Updated the value {} -> {}",
                    thread::current().id(),
                    task_id,
                    v0,
                    v1,
                );
                Ok(())
            })
    })
}

struct MyDataUpdater {
    task_id: u8,
    data: Arc<Mutex<u8>>,
}

impl MyDataUpdater {
    fn new(task_id: u8, data: Arc<Mutex<u8>>) -> Self {
        Self { task_id, data }
    }
}

impl Future for MyDataUpdater {
    type Item = Arc<Mutex<u8>>;
    type Error = ();

    fn poll(&mut self) -> Result<Async<Self::Item>, Self::Error> {
        debug!("{:?} Task({}) - Polled", thread::current().id(), self.task_id);
        Ok(Async::Ready(Arc::clone(&self.data)))
    }
}
