use futures::Future;
use futures03::lock::Mutex;
use log::debug;
use rand::Rng;
use std::{
    error::Error,
    sync::Arc,
    thread,
    time::{Duration, Instant},
};
use tokio::{
    runtime::{Builder as RuntimeBuilder, Runtime},
    timer::Delay,
};

fn main() -> Result<(), Box<dyn Error>> {
    // For converting futures 0.3 Futures into futures 0.1 Futures.
    use futures03::future::{FutureExt, TryFutureExt};

    env_logger::init();

    // Number of concurrent async updater tasks to run.
    const NUM_TASKS: u8 = 10;

    // Create the shared u8 value protected by the mutex.
    let shared_value = Arc::new(Mutex::new(0));

    // Create async update tasks.
    let mut tasks = (0..NUM_TASKS)
        .map(|i| {
            update(i, Arc::clone(&shared_value))
                // Convert futures 0.3 Future into futures 0.1 Future.
                .boxed()
                .compat()
        })
        .collect::<Vec<_>>();
    let last_task = tasks.pop().ok_or_else(|| "Failed to get the last task")?;

    // Run the async update tasks in parallel. (well, actually sequentially due to the mutex)
    let mut runtime = build_runtime()?;
    tasks.into_iter().for_each(|task| {
        runtime.spawn(task);
    });
    runtime
        .block_on_all(last_task)
        .map_err(|()| "Failed to run updaters")?;

    // Verify the final value.
    let final_value = Arc::try_unwrap(shared_value)  // Get the enclosed Mutex<u8>.
        .map_err(|_| "Failed to unwrap the Arc")?
        .into_inner();                               // Get the enclosed u8.
    debug!("The final value is {}.", final_value);
    assert_eq!(final_value, NUM_TASKS);

    Ok(())
}

fn build_runtime() -> Result<Runtime, Box<dyn Error>> {
    RuntimeBuilder::new()
        .panic_handler(|err| std::panic::resume_unwind(err))
        .build()
        .map_err(|err| err.into())
}

async fn update(task_id: u8, shared_value: Arc<Mutex<u8>>) -> Result<(), ()> {
    // For converting futures 0.1 Futures into futures 0.3 Futures
    use futures03::compat::Future01CompatExt;

    debug!("{:?} Task({}) - Started", thread::current().id(), task_id);

    // Acquire the lock asynchronously.
    let mut guard = shared_value.lock().await;
    debug!(
        "{:?} Task({}) - Acquired the lock: {:?}",
        thread::current().id(),
        task_id,
        guard,
    );

    // Get the shared u8 value. (This will clone the u8 value)
    let v0 = *guard;

    // Do a time-consuming async work.
    let delay_millis = rand::thread_rng().gen_range(50, 100);
    Delay::new(Instant::now() + Duration::from_millis(delay_millis))
        .map_err(|_err| ())
        .compat()  // Convert to futures 0.3 Future.
        .await?;

    // Update the shared u8 value using the former value retrieved sometime ago.
    let v1 = v0 + 1;
    *guard = v1;
    debug!(
        "{:?} Task({}) - Updated the value {} -> {}",
        thread::current().id(),
        task_id,
        v0,
        v1,
    );
    Ok(())
} // The guard will be dropped here.
