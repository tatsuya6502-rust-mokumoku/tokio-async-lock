use futures::Future;
use log::debug;
use rand::Rng;
use std::error::Error;
use std::thread;
use std::time::{Duration, Instant};
use tokio::prelude::Async;
use tokio::runtime::{Builder as RuntimeBuilder, Runtime};
use tokio::sync::lock::{Lock, LockGuard};
use tokio::timer::Delay;

fn main() -> Result<(), Box<dyn Error>> {
    env_logger::init();

    // Number of concurrent async updater tasks to run.
    const NUM_TASKS: u8 = 10;

    // Create the shared u8 value protected by the lock.
    let lock = Lock::new(0);

    // Create async update tasks.
    let mut tasks = (0..NUM_TASKS)
        .map(|i| update_task(i, lock.clone()))
        .collect::<Vec<_>>();
    let last_task = tasks.pop().ok_or_else(|| "Failed to get the last task")?;

    // Run the async update tasks in parallel. (well, actually sequentially due to the lock)
    let mut runtime = build_runtime()?;
    tasks.into_iter().for_each(|task| {
        runtime.spawn(task);
    });
    runtime
        .block_on_all(last_task)
        .map_err(|()| "Failed to run updaters")?;

    // Verify the final value.
    let v = build_runtime()?
        .block_on_all(read_task(lock))
        .map_err(|()| "Failed to run a reader")?;
    debug!("The final value is {}.", v);
    assert_eq!(v, NUM_TASKS);

    Ok(())
}

fn build_runtime() -> Result<Runtime, Box<dyn Error>> {
    RuntimeBuilder::new()
        .panic_handler(|err| std::panic::resume_unwind(err))
        .build()
        .map_err(|err| err.into())
}

fn update_task(task_id: u8, lock: Lock<u8>) -> impl Future<Item = (), Error = ()> {
    // Acquire the lock asynchronously.
    MyDataUpdater::new(task_id, lock)
        .and_then(move |mut guard| {  // need to move the task_id into this closure.
        // Get the shared u8 value. (This will clone the u8 value)
        let v0 = *guard;

        // Do a time-consuming async work.
        let delay_millis = rand::thread_rng().gen_range(50, 100);
        Delay::new(Instant::now() + Duration::from_millis(delay_millis))
            .map_err(|_err| ())
            .and_then(move |_| {
                // The guard could be moved into a closure.

                // Once the async work is done, update the shared u8 value
                // using the former value retrieved sometime ago.
                let v1 = v0 + 1;
                *guard = v1;
                debug!(
                    "{:?} Task({}) - Updated the value {} -> {}",
                    thread::current().id(),
                    task_id,
                    v0,
                    v1,
                );
                drop(guard);
                Ok(())
            })
    })
}

fn read_task(lock: Lock<u8>) -> impl Future<Item = u8, Error = ()> {
    MyDataUpdater::new(99, lock).and_then(|guard| Ok(*guard))
}

struct MyDataUpdater {
    task_id: u8,
    data: Lock<u8>,
}

impl MyDataUpdater {
    fn new(task_id: u8, data: Lock<u8>) -> Self {
        Self { task_id, data }
    }
}

impl Future for MyDataUpdater {
    type Item = LockGuard<u8>;
    type Error = ();

    fn poll(&mut self) -> Result<Async<Self::Item>, Self::Error> {
        debug!("{:?} Task({}) - Polled", thread::current().id(), self.task_id);
        match self.data.poll_lock() {
            Async::Ready(guard) => {
                debug!(
                    "{:?} Task({}) - Acquired the lock: {:?}",
                    thread::current().id(),
                    self.task_id,
                    guard,
                );
                Ok(Async::Ready(guard))
            }
            Async::NotReady => Ok(Async::NotReady),
        }
    }
}
