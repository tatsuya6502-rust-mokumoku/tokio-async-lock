<!-- -*- coding:utf-8-unix -*- -->

# Rust: Tokio Async Lock

Tokioの非同期ロックのサンプル / An Async Lock Example with Tokio

ある既存のコードベース（Tokio 0.1、futures 0.1を使用）で、複数の非同期タスクから同時に更新される共有リソースを守るために、複数の`Future`にまたがったロックが必要になった。
とりあえず、Tokio 0.1とfutures 0.1のままで実現する方法については、Doraさんがコードを書いて実験している。

ここでは、Doraさんの実験コードをベースに、Rust 1.39で導入された`async/.await`ならどう書けるのか調査した。
また、Tokio 0.1＋futures 0.1と、std::future/futures 0.3との相互運用性についても確認した。

## TL;DR

- `async/.await`を用いると簡潔に書けることがわかった
    - `Future`トレイトを実装した独自の型（例：`MyDataUpdater`）を手で書かずにすむ
    - `async/.await`全般の話として、`and_then()`などのコンビネータの利用が減るのでコードの見通しが良くなる。また、今回は確認できなかったが、ライフタイムも扱いやすくなるはず
- ロックが複数の`Future`にまたがるときは非同期に対応したロックが必須となる
    - 非同期に未対応のロック（例：`std::sync::{Mutex, RwLock}`）を使用すると非同期ランタイムのスケジューラやエグゼキュータがデッドロックする危険があるとのこと
    - 非同期に対応したロックの例
        - futures 0.3 &mdash; `futures::lock::Mutex`
        - Tokio 0.2 &mdash; `tokio::sync::Mutex`
        - Tokio 0.1 &mdash; `tokio::sync::Lock`
        - async-std &mdash; `async_std::sync::{Mutex, RwLock}`
- Tokio 0.1、futures 0.1とstd::future/futures 0.3との相互運用性は問題なさそう


## 元のコード

Doraさんの [この実験コード][dora-gt-tokio-future-lock] をベースにした。
Tokio 0.1の`tokio::sync::Lock`を使用している。

[dora-gt-tokio-future-lock]: https://github.com/dora-gt/tokio-future-lock

## 環境

### 既存の環境

- Rust 2018 Edition（Rustバージョンは不明）
- Tokio 0.1.22ランタイム
- Futures 0.1.29

### 今回テストした環境

- Rust 1.39.0（`async/.await`に対応した最初のバージョン）
- Tokio 0.1.22ランタイム
- Futures 0.1.29
- std::future（`async/.await`で必要）
- Futures 0.3（非同期対応のMutex、および、futures 0.1とstd::feature/futures 0.3との相互変換）

## 非同期に対応したロックについて

**TODO**

## Futures 0.1/0.3の相互運用性について

**TODO**

- `async fn`をfutures 0.1の`Future`に変換してTokio 0.1のランタイムで実行したり、逆にfutures 0.1の`Future`を`async fn`内で`.await`したりできる
- future 0.3と0.1の両方を`use`したいときはCargoのdependencyリネーム機能を使う

Futures.rsプロジェクトのブログ記事 [Futures 0.1 Compatibility Layer][futures-interop] が参考になる。

[futures-interop]: https://rust-lang-nursery.github.io/futures-rs/blog/2019/04/18/compatibility-layer.html

## サンプルプログラムの内容

3つのプログラムを制作した。

1. [main1.rs][main1] &mdash; ほぼ元の実験コード（futures 0.1）
2. [main2.rs][main2] &mdash; `main1`と同じ構成だが、ロックがないため問題が起こるコード（futures 0.1）
3. [main3.rs][main3] &mdash; `async/.await`ベースの実験コード（std::future, futures 0.3）

### その1：ほぼ元の実験コード

- [`src/bin/main1.rs`][main1]

Doraさんの元の実験コードをできるだけ変えずに、もしロックがなかったら確実にrace conditionの問題が起こるようにした。

- 共有リソース（`u8`型）の値を`+=`や`*=`で更新するのではなく、次のように共有リソースの値の取得と更新の間に時間がかかるようにした
    1. 共有リソースのロックを取得
    2. 共有リソースの値を取得
    3. 遅延時間が50msから100ms程度の非同期タスクを実行
    4. 2.で取得した値に`+1`して、それを共有リソースに設定
    5. 共有リソースのロックを解除
- 上記の処理を行うタスクを10個同時に実行して、最後に答えを確認（`10`が正解）

### その2：ロックがないため問題の起こるコード

- [`src/bin/main2.rs`][main2]

`main1`のコードからロックをなくしてrace conditionの問題が起こるようにした。
間違った答え（`1`）になる。

### その3：`async/.await`ベースのコード

- [`src/bin/main3.rs`][main3]

`main1`のコードをベースに、`async/.await`とfutures 0.3の`lock::Mutex`で書き換えたもの。
コードが20%ほど短くなったし、内容も分かりやすくなったと思う。

[main1]: ./src/bin/main1.rs
[main2]: ./src/bin/main2.rs
[main3]: ./src/bin/main3.rs

## サンプルプログラムの実行方法

```console
$ cargo build --release
$ RUST_LOG=debug ./target/release/main1
$ RUST_LOG=debug ./target/release/main2
$ RUST_LOG=debug ./target/release/main3
```

## オープンソースライセンス

MITライセンス
